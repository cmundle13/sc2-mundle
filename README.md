#How Connected?
## By: Caleb Mundle
* * *
Technology is connected to us in many ways, just as we are connected to technology in many ways; we control technology, just as it controls us. This comes from our pursuit of information.

- Steven Shaviro's Zeroes and Ones details the importance of recognizing that "the dilemma of information management is a binary one: how to accumulate information on the one hand, and how to limit access to it, on the other hand."

Information surrounds us in our search of information. We use technology for the purpose of gaining information, which explains technologies huge role in our culture today.
* * *
![](https://png.pngtree.com/png-clipart/20190603/original/pngtree-beautiful-stars-png-image_193326.jpg)

* * *

##Limited Access
* * *
With the acquisition of information at the helm of technologies' purpose, it is important to ask why we're spending time (and money) gathering this information.
- "Value results from abundance, not from scarcity." (Shaviro, Zeroes and Ones)

So, as information increases in abundance, so will it's value. This is why Shaviro says that wealthy interests are using trademarks, copyright, and patents in order to limit the publics access to certain information. Information acquisition works a lot like property acquisition worked in past centuries.

- The wealthy and powerful use their existing power to buy land (or in this case, information) in order to increase the wealth/power they already have.
* * * 
![](https://png.pngtree.com/png-clipart/20190603/original/pngtree-man-sun-money-happy-png-image_208820.jpg)
* * * 

##What Lies Ahead
Industries such as google and youtube control a large amount of information wherein technology is used to identify and store said info... Because of their massive wealth, it seems that this trend is likely to continue. Just as many governments control what their citizens can/cannot see, powerful and private interests on the internet control much of the information that we are able/not able to see.

![](https://png.pngtree.com/png-clipart/20190604/original/pngtree-vector-puppet-png-image_901401.jpg)